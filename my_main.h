//
// Created by grostig on 10/11/17.

#pragma once
#include "cppheaders.h"
//#include <boost/tuple/tuple.hpp>
// This must be defined before the first time that "gnuplot-iostream.h" is included.
#define GNUPLOT_ENABLE_PTY
#include "gnuplot-iostream.h"

#include <eigen3/Eigen/Dense>

using Eigen::VectorXd;
using Eigen::MatrixXd;

/*
using C_lcb = Eigen::Vector3d;  // Coefficient in linear equation
using V_lcb = Eigen::Vector3d;  // Variable x's in linear equation OR Sample Vector // had problems in declaring a variable with VectorXd(3) below  todo
using E_lcb = Eigen::Vector3d;  // Exponents on the x's in linear equation
using D_lcb = Eigen::Vector3d;  // Divisor for algebra of linear equation conversions.

using X_lcb = Eigen::Vector3d;  // Variable x's in linear equation OR Sample Vector // had problems in declaring a variable with VectorXd(3) below  todo
using W_lcb = Eigen::Vector3d;  // Weight vector
*/
using C_lcb = Eigen::Vector3d;  // Coefficient in linear equation // not used in bias_variance
using V_lcb = Eigen::Vector3d;  // Variable x's in linear equation OR Sample Vector // had problems in declaring a variable with VectorXd(3) below  todo // not used in bias_variance

using E_lcb = Eigen::VectorXd;  // Exponents on the x's in linear equation

using D_lcb = Eigen::Vector3d;  // Divisor for algebra of linear equation conversions.  // not used in bias_variance

using X_lcb = Eigen::VectorXd;  // Variable x's in linear equation OR Sample Vector // had problems in declaring a variable with VectorXd(3) below  todo
using W_lcb = Eigen::VectorXd;  // Weight vector

//using W_lcb_zero = Eigen::VectorXd::Zero(2); todo why not? is it a function?

#include "linear_equation.h"
#include "standard_form_lin_eq.h"
#include "slope_intercept_lin_eq.h"
//#include "E_too_many_attempts.h"







