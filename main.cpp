/* Linear_Regression/main.cpp - Provides boiler-plate code.

   Copyright (c) 2017 Grant Rostig. All rights reserved. Use by written permission only, evidenced with blue ink on paper or a verifyable (pgp) email interaction with Grant Rostig.
   Permission is granted to use these files during and in support of CppMSG meetup group events and projects.

   Inspired by: Caltech LFD

   Shows how to:
   -fit a line to classified? data todo
   -
   -



// #include "../gsl/gsl"   // Standard C++ Foundation: C++14 Guidelines Support Library

// Variable naming convention to correspond to mathematical use of fonts.
// lc=Lower Case, uc=Upper Case, s=script (ie.not block characters), b=bold
*/
#include "my_main.h"

constexpr int perceptron_algorithm =                1;  //  Q, HW1
constexpr int linear_ols_algorithm =                2;  // Question 5, HW2
constexpr int e_out_linear_ols_algorithm =          3;  // Question 6, HW2 - not fully implemented
constexpr int ols_to_perceptron_algorithm =         4;  // Question 7, HW2 - not fully implemented
constexpr int transformed_linear_ols_algorithm =    5;  // Question 8,9,10,  HW2 - not fully implemented
constexpr int bias_variance_analysis_ols_algorithm= 6;  // Question 4,5,6,   HW4 - not fully implemented
constexpr int algorithm =                           6;  // *** Selection of which one to run.
constexpr bool update_random_point_perceptron =     false; // true is perceptron should choose a random misclassification else first one.

constexpr long vector_size = 1;
constexpr long n_uc_num_samples = 2;
constexpr long num_desired_trials = 1000000;  // Run multiple times to get averaged numbers.
constexpr double per_cent_noise = 10.0;   // 10% = 10.0  NOTE: this feature is only intended to be use with OLS or Pocket (which is not implemented yet)  not with the regular perceptron.
// Internal details - no need to edit
constexpr long num_desired_propability_tests = 1000;
constexpr long num_desired_e_out_tests = 1000;
constexpr long sleep_micro_seconds = 150*1000;
constexpr long max_update_attempts = (num_desired_trials * 10000);  // to avoid endless loops if bugs. Number may need adjusting for difficulty.
constexpr double sample_space_upper_bound = 1;
constexpr double sample_space_lower_bound = -1;
constexpr double GREEN = +1;
constexpr double BLUE = -1;
//enum class Point_color:int {
//    GREEN = 1,
//    BLUE = -1
//};
constexpr double sample_space_upper_bound2 = 1000;
constexpr double sample_space_lower_bound2 = -1000;
static const double grostig_pi = 3.141592653589793;

class E_too_many_attempts: public std::exception {
    virtual const char* what() const throw()
    {
        return "Too_many_attempts_exception happened";
    }
} e_too_many_attempts;

std::random_device my_random_device;
std::mt19937 gen(my_random_device());
std::vector<double> interval{sample_space_lower_bound, sample_space_upper_bound};
std::vector<double> density_weight{1, 0, 1}; // huh??? TODO
std::piecewise_constant_distribution<> my_distribution (interval.begin(), interval.end(), density_weight.begin());

std::random_device my_random_device2;
std::mt19937 gen2(my_random_device2());
std::vector<double> interval2{sample_space_lower_bound2, sample_space_upper_bound2};
std::vector<double> density_weight2{1, 0, 1}; // huh??? TODO
std::piecewise_constant_distribution<> my_distribution2 (interval.begin(), interval.end(), density_weight.begin());

std::random_device my_random_integer_device;
std::mt19937 my_random_integer_engine(my_random_integer_device());
std::uniform_int_distribution<int> my_integer_distribution(0, n_uc_num_samples-1);
//long random_integer_offset = my_integer_distribution(my_random_integer_engine);

/*
Slope_intercept_lin_eq slope_intercept_form_2d_lin_eq(Standard_form_lin_eq l) {
     *
     * separate either y or z
     *
    Slope_intercept_lin_eq r = {};
    if (l.coefficients[3] == 0 ) // && l.exponents[3] == 0 ) // and all the rest too todo // verify not doing 3d.
    {
        assert (l.coefficients[2] != 0 && l.exponents[2] != 0 && l.divisors[2] != 0);
        // move out x2 (ie. y axis) to LHS

       // make y positive by flipping sign of RHS
        if (l.coefficients[2] > 0) {
            for (long j = 0; j < l.coefficients.size(); j++) {
                l.coefficients[j] += -1;
            }
        }
        l.coefficients[2] = 0;
        l.exponents[2] = 0;
        l.divisors[2] = 0;



    }
    return r;
}


Slope_intercept_lin_eq standard_form_lin_eq(Slope_intercept_lin_eq l) {

}
    */

class X_lcb_sample {  // contains the features of one sample.  called data vector in linear algebra
public:
    X_lcb x_lcb = Eigen::VectorXd::Zero(vector_size);
    // X_lcb x_lc2(2); why won't this work???
    double y_true_sample_label = 0;
    double h_temp_sample_label = 0;
    X_lcb_sample() {
        x_lcb << my_distribution(my_random_device); // features of one sample, in this case without the x0 term of 1 for the constant intercepts.
    }
/*                                                       X_lcb_sample()  // Constructor initializes all samples to random values.
                                                      : x_lcb[0] { my_distribution(my_random_device) } // why doesn't this work???
                                                      x2 { my_distribution(my_random_device) }
                                                   { cout << "inside X_lcb_sample.constructor: " << std::endl; };   */
};
using D_ucs = std::vector<X_lcb_sample>;

double roundp2(double x) {
    return round(x*100)/100;
}
/*
void plot_graph( Gnuplot & gp, std::string title, D_ucs & d_ucs, W_lcb & w_lcb, E_lcb & f_exponents) {  // , double m0, double m1, double m2) {
//     Plot the current samples and classifier boundary line
//     https://github.com/matplotlib/matplotlib/issues/7684/
//     https://stackoverflow.com/questions/30002058/gnuplot-3d-plot-scatter-points-and-a-surface-and-lines-between-the-them
//     https://i.stack.imgur.com/pnthj.png
//     http://gnuplot.sourceforge.net/demo/scatter.html
//     * http://psy.swansea.ac.uk/staff/carter/gnuplot/gnuplot_3d.htm
//     * http://gnuplot.sourceforge.net/demo/surface1.html

    std::vector<std::tuple<double,double> > green_points;
    std::vector<std::tuple<double,double> > red_points;
    for( auto i : d_ucs ) {
        if ( i.y_true_sample_label == 1.0 ) {
            green_points.push_back(std::make_tuple(i.x_lcb[1], i.x_lcb[2]));
        } else {
            red_points.push_back(std::make_tuple(i.x_lcb[1], i.x_lcb[2]));
        }
    }
//    double mx=0.5, my=0.5;
//    int mb=1;
    gp << "set title '"<< title <<"'\n set size ratio -1\n";
    gp << "set xlabel 'x_1'\n set ylabel 'x_2'\nset xrange [-1:3]\nset yrange [-1:1]\n";
    gp << "plot '-' with points title 'pass:', '-' with points title 'fail:' ";
    if ( algorithm == transformed_linear_ols_algorithm ) {
        gp << std::setprecision(2) << std::fixed
           << ", sqrt("
           << roundp2(-1.0*w_lcb[1])<<"*x**"<< f_exponents[1]
           << "+"<<roundp2(-1*w_lcb[0])
           << ") ";
        cout << std::setprecision(2) << std::fixed
           << ", sqrt("
           << roundp2(-1.0*w_lcb[1])<<"*x**"<< f_exponents[1]
           << "+"<<roundp2(-1*w_lcb[0])
           << ") ";
        gp << std::setprecision(2) << std::fixed
           << ", -sqrt("
           << roundp2(-1.0*w_lcb[1])<<"*x**"<< f_exponents[1]
           << "+"<<roundp2(-1*w_lcb[0])
           << ") \n";
        cout << std::setprecision(2) << std::fixed
           << ", -sqrt("
           << roundp2(-1.0*w_lcb[1])<<"*x**"<< f_exponents[1]
           << "+"<<roundp2(-1*w_lcb[0])
           << ") \n";
    } else {
        //       <<-m1/m2<<"*x+"<<-1*m0/m2<<" , "
        gp << std::setprecision(2) << std::fixed
           << roundp2(-w_lcb[1]/w_lcb[2])<<"*x+"<<roundp2(-1*w_lcb[0]/w_lcb[2])<<" \n";
    }

    gp.send1d(green_points);
    gp.flush();  // may not be necessary
    gp.send1d(red_points);
    gp.flush();
    usleep(sleep_micro_seconds); // is there a more modern way? todo
//    gp.getMouse(mx, my, mb, "Left click to aim arrows, right click to exit.");
//    printf("You pressed mouse button %d at x=%f y=%f\n", mb, mx, my);
//    if(mb < 0) printf("The gnuplot window was closed.\n");
}
*/

void plot_graph_bias_variance( Gnuplot & gp, std::string title, D_ucs & d_ucs, W_lcb & w_lcb, E_lcb & f_exponents /*, double m0, double m1, double m2*/) {
/*     Plot the current samples and classifier boundary line
     https://github.com/matplotlib/matplotlib/issues/7684/
     https://stackoverflow.com/questions/30002058/gnuplot-3d-plot-scatter-points-and-a-surface-and-lines-between-the-them
     https://i.stack.imgur.com/pnthj.png
     http://gnuplot.sourceforge.net/demo/scatter.html
     * http://psy.swansea.ac.uk/staff/carter/gnuplot/gnuplot_3d.htm
     * http://gnuplot.sourceforge.net/demo/surface1.html
     * */
    std::vector<std::tuple<double,double> > green_points;
    for (auto i  : d_ucs){
        green_points.push_back(std::make_tuple(i.x_lcb[0], i.y_true_sample_label));
    }
    gp << "set title '"<< title <<"'\n set size ratio -1\n";
    gp << "set xlabel 'x_1'\n";
    gp << "set ylabel 'x_2'\n";
    gp << "set xrange [-1:3]\n";
    gp << "set yrange [-1:1]\n";
    gp << "plot '-' with points title 'pass:' ";
//    gp << ", '-' with points title 'fail:' ";
    // *** Plot the lines ***
    if ( algorithm == bias_variance_analysis_ols_algorithm ) { // first plot the f() line
        gp << std::setprecision(2) << std::fixed
           << ", sin("
           << "pi*x**"<< f_exponents[0] // update this to use coefficient not pi. todo
           << ") ";
        gp << " ";
        // then: plot the g() line
        gp << std::setprecision(2) << std::fixed
           << ", "
           << roundp2(w_lcb[0])<<"*x**"<< f_exponents[0];
        gp << " \n";
    }
/*
    else if ( algorithm == transformed_linear_ols_algorithm ) {
        gp << std::setprecision(2) << std::fixed
           << ", sqrt("
           << roundp2(-1.0*w_lcb[1])<<"*x**"<< f_exponents[1]
           << "+"<<roundp2(-1*w_lcb[0])
           << ") ";
        cout << std::setprecision(2) << std::fixed
           << ", sqrt("
           << roundp2(-1.0*w_lcb[1])<<"*x**"<< f_exponents[1]
           << "+"<<roundp2(-1*w_lcb[0])
           << ") ";
        gp << std::setprecision(2) << std::fixed
           << ", -sqrt("
           << roundp2(-1.0*w_lcb[1])<<"*x**"<< f_exponents[1]
           << "+"<<roundp2(-1*w_lcb[0])
           << ") \n";
        cout << std::setprecision(2) << std::fixed
           << ", -sqrt("
           << roundp2(-1.0*w_lcb[1])<<"*x**"<< f_exponents[1]
           << "+"<<roundp2(-1*w_lcb[0])
           << ") \n";
    } else {
        //       <<-m1/m2<<"*x+"<<-1*m0/m2<<" , "
        gp << std::setprecision(2) << std::fixed
           << roundp2(-w_lcb[1]/w_lcb[2])<<"*x+"<<roundp2(-1*w_lcb[0]/w_lcb[2])<<" \n";
    }
*/
    gp.send1d(green_points);  // now plot the points
    gp.flush();  // may not be necessary
    usleep(sleep_micro_seconds); /* is there a more modern way? todo */
/*    gp.getMouse(mx, my, mb, "Left click to aim arrows, right click to exit.");
    printf("You pressed mouse button %d at x=%f y=%f\n", mb, mx, my);
    if(mb < 0) printf("The gnuplot window was closed.\n");*/
}

void add_noise( D_ucs & d_ucs, const double per_cent ) {
    long num = static_cast<double>(n_uc_num_samples) * per_cent / 100.0; // truncation is intended.  We need an integer. todo
    std::vector<long> noisy = {};
    for (long i=0; i<n_uc_num_samples; i++) {  // pythonic way to do this?  todo
        noisy.push_back(i);
    }
    std::shuffle(noisy.begin(), noisy.end(), my_random_integer_engine);
//    cout<< "\nbefore swap: " << d_ucs << endl;
    auto swap_sign = [& d_ucs](auto & x) {
        d_ucs[x].y_true_sample_label *= -1.0;
    };
    std::for_each(noisy.begin(), noisy.begin()+num, swap_sign);
//    cout<< "\nafter swap: " << d_ucs << endl;  // template error!! todo
}
/*
double label_sample_dot_sign(const W_lcb & f_coefficients, const X_lcb & x_lcb_sample) {
    auto sum = f_coefficients.dot( x_lcb_sample ); // dot product of two vectors
    return (sum >= 0) ? GREEN : BLUE;  // sign() threshold (AKA. activation) function.
}

double label_sample_expnt_sign(const W_lcb &f_coefficients, const E_lcb &f_exponents, X_lcb x_lcb_sample) {

//     NOTE:  We do want a copy of x_lcb_sample, because we may mutate it in here and don't want that outside of this f()
////    auto sum = f_coefficients.dot( x_lcb_sample ); // dot product of two vectors

    x_lcb_sample = x_lcb_sample.array().pow( f_exponents.replicate(1, f_exponents.cols()).array() );
    double sum = f_coefficients.dot( x_lcb_sample );
    return (sum >= 0) ? GREEN : BLUE;  // sign() threshold (AKA. activation) function.
}

double count_errors(W_lcb f_coefficients, W_lcb w_lcb_weights) {  // utility function for reporting purposes.
    long error_probabilty_samples = {0};
    for (int i = 1; i < num_desired_propability_tests; i++) {
//        /* test random point against g()
        X_lcb random_sample = {1, my_distribution(my_random_device), my_distribution(my_random_device)};
        // label the random sample with unknown f()
        double true_label = label_sample_dot_sign(f_coefficients, random_sample);
        double classified_label = label_sample_dot_sign(w_lcb_weights, random_sample);
//                                                                  cout << " sum, color: " << sum << ", " << x_lcb_of_t.h_temp_sample_label << std::endl;
        if (classified_label != true_label) { error_probabilty_samples++; }
    }
    return error_probabilty_samples;
}

W_lcb perceptron( D_ucs& d_ucs, W_lcb& w_lcb_weights, E_lcb f_exponents, long& algorithm_w_lcb_updates, Gnuplot& plot_canvas) {
    bool any_misclassified = {};
    do {
        any_misclassified = false;
        long num_misclassied_points = {0};
        for ( auto & x_lcb_of_t : d_ucs ) { // Look for one sample that is misclassified by current weight vector
//            /* Classify sample
//             * classify based on current w *
//             * double h_lc_of_x_t = label_sample_dot_sign(w_lcb_weights, x_lcb_of_t.x_lcb);
            x_lcb_of_t.h_temp_sample_label = h_lc_of_x_t;  // Storing the current classification for reporting purposes only.
//            cout << "\n> x1, truecolor, classcolor: "<<x_lcb_of_t.x_lcb[1]<<", "<< x_lcb_of_t.y_true_sample_label<<", "<< x_lcb_of_t.h_temp_sample_label;

            if (h_lc_of_x_t != x_lcb_of_t.y_true_sample_label)
            {
                if (max_update_attempts < ++algorithm_w_lcb_updates) { cout << std::endl;cout.flush();throw e_too_many_attempts;} // c++ details, why can't I flush cout before dump??? todo
                num_misclassied_points++;
                any_misclassified = true;
                if (!update_random_point_perceptron) {
//                                  cout << "\n>> algorithm_w_lcb_updates: " << algorithm_w_lcb_updates << "// ";
                    /* *** Update weight vector ***
                     * calculate temp  = y(t)*x(t) without mutating x_lcb
                     * This is the magical step where the color's sign is used to create an incremental
                     * update to be applied to w *
//                                                        cout << " scalar * x: ";
// //                                                     cout<< "\n >> add w adjustment/old w vec: " << w_lcb_weights << ", "<< temp<<" ";
////                                                      cout << ">new w vec: " << w_lcb_weights << x_lcb_of_t.x_lcb << " // truecolor, classified: " << x_lcb_of_t.y_true_sample_label << ", " << x_lcb_of_t.h_temp_sample_label << std::endl;
                    w_lcb_weights += (x_lcb_of_t.y_true_sample_label * x_lcb_of_t.x_lcb);
                    plot_graph( plot_canvas, "Perceptron", d_ucs, w_lcb_weights, f_exponents );
                }
            };
        }
        if (update_random_point_perceptron && ( num_misclassied_points > 0 ) )
        {
            std::random_device my_random_device;
            std::mt19937 my_random_num_engine(my_random_device());
            std::uniform_int_distribution<int> my_distribution(0, n_uc_num_samples-1);
            long random_integer_offset = my_distribution(my_random_num_engine);
//                                      cout << "\n> random_perceptron_update// num_misclassied_points, random_integer_offset: "<<num_misclassied_points<<", "<<random_integer_offset;
//            /* https://stackoverflow.com/questions/17865488/find-the-nth-element-satisfying-a-condition *
            std::vector<X_lcb_sample>::iterator nth = d_ucs.begin() + random_integer_offset;
            auto equal = [](const X_lcb_sample & x) {
//                                      cout << "\n > equal: "<< (x.h_temp_sample_label == x.y_true_sample_labeXXXXXl);
                return (x.h_temp_sample_label == x.y_true_sample_label);
            };
            auto misclassified_sample_it = std::find_if_not(nth, d_ucs.end(), equal );
            if ( d_ucs.end() == misclassified_sample_it ) {  /* in case we run off the end looking for a misclassified sample, search from beginning *
//                cout << "\n< before my offset :";
                misclassified_sample_it = std::find_if_not( d_ucs.begin(), d_ucs.end(), equal);
                assert ( d_ucs.end() != misclassified_sample_it );
            }
            w_lcb_weights += (misclassified_sample_it->y_true_sample_label * misclassified_sample_it->x_lcb);
            plot_graph( plot_canvas, "Perceptron", d_ucs, w_lcb_weights, f_exponents );
        }
    } while (any_misclassified);
    return w_lcb_weights;
}

W_lcb ordinary_least_squares_x3( D_ucs & d_ucs ) {  // Linear regression algorithm
//     *
//     * Construct X and y
//     *
    MatrixXd x_uc(n_uc_num_samples,3); // = Eigen::MatrixXd::Zero(n_uc_num_samples, 3);
    long j = 0;
    for ( auto i : d_ucs ) {
        x_uc.row(j++) += i.x_lcb.transpose();  // why don't I need to do a transpose() here? helper functions :( eigen block
    }

//    cout << "\n> X,y: \n" << x_uc;
    VectorXd y_lcb = Eigen::VectorXd::Zero(n_uc_num_samples);
    j = 0;
    for ( auto i : d_ucs ) {
        y_lcb(j++) = i.y_true_sample_label;
    }
//    cout << "\n> y_lcb: "<<y_lcb.transpose() << endl;
//    MatrixXd x1 = x_uc.transpose() * x_uc;
//    cout << "\n> X1: \n" << x1;
//    MatrixXd x2 = x1.inverse();
//    cout << "\n> X2: \n" << x2;
//    MatrixXd x3 = x2 * x_uc.transpose();
//    cout << "\n> X3: \n" << x3;
//    x_uc = x3;
    x_uc = ( x_uc.transpose() * x_uc ).inverse() * x_uc.transpose();  // X hat calculation
    W_lcb temp = x_uc * y_lcb;
//    cout << "\n> return: \n" << temp.transpose();
    return temp;
};

*/

W_lcb ordinary_least_squares_real( D_ucs & d_ucs ) {  // Linear regression algorithm

//     Construct X and y

    MatrixXd x_uc(n_uc_num_samples, vector_size); // = Eigen::MatrixXd::Zero(n_uc_num_samples, 3);
    long j = 0;
    for ( auto i : d_ucs ) {
        x_uc.row(j++) += i.x_lcb.transpose();  // why don't I need to do a transpose() here? helper functions :( eigen block
    }

//    cout << "\n> X,y: \n" << x_uc;
    VectorXd y_lcb = Eigen::VectorXd::Zero(n_uc_num_samples);
    j = 0;
    for ( auto i : d_ucs ) {
        y_lcb(j++) = i.y_true_sample_label;
    }
//    Perform Linear Regression to calcuate the weight vector
/*
    cout << "\n> y_lcb: "<<y_lcb.transpose() << endl;
    MatrixXd x1 = x_uc.transpose() * x_uc;
    cout << "\n> X1: \n" << x1;
    MatrixXd x2 = x1.inverse();
    cout << "\n> X2: \n" << x2;
    MatrixXd x3 = x2 * x_uc.transpose();
    cout << "\n> X3: \n" << x3;
    x_uc = x3;
*/
    x_uc = ( x_uc.transpose() * x_uc ).inverse() * x_uc.transpose();  // X hat calculation  // which has as a part the pseudo inverse?
    W_lcb temp = x_uc * y_lcb;
//    cout << "\n> return: \n" << temp.transpose();
    return temp;
};

double calc_g_mean (std::vector<W_lcb> const & historical_g_lcs) {
    double g_sum = 0;
    for (size_t i=0; i<num_desired_trials; i++){  // redo with range for todo
        W_lcb temp = historical_g_lcs[i];
        g_sum += temp(0);
    }
    return g_sum / num_desired_trials;
}

int main() {
    long algorithm_w_lcb_updates = {0};
    long error_probabilty_samples_total = {0};
    std::vector<W_lcb> historical_g_lcs = {}; // keep history of multible trials
    std::vector<W_lcb> historical_f_lcs = {};
    std::vector<D_ucs> historical_d_ucs = {};
    Gnuplot plot_canvas;

    for ( long num_trials=1; num_trials <= num_desired_trials; num_trials++ ) {

        D_ucs d_ucs(n_uc_num_samples); // Build data set with initial size.
        /*
         * For our starting unknown f(), the Decision Boundary equation of the linear boundary hyperplane.
           We will do the initial marking of our samples with this hyperplane.
           y = f(x1,x2) = m0*x0 + m1*x1 + m2*x2, where x0=1 which makes the m0*x0 term the line intercept constant.
           Example 1 f() is:
           boundary line:       y = 2x + .01
           Green region is:     y >= x + .01
                                0 >= -y + x + .01
                                0 <= +y -x -.01
                                + m0*x0 + m1*x1 + m2*x2 >= 0
           ( (m0, m1, m2) dot (1, x1, x2) ) >= 0

           Example 2 f() is:
           boundary line:       y =  -+sqrt(- x**2 + 0.6)
           Green region is:     y >= -+sqrt(- x**2 + 0.6)
                                y**2 >=     - x**2 + 0.6
                                0 >= - y**2 - x**2 + 0.6
                                0 <= + y**2 + x**2 - 0.6
                                + -1*0.6 + 1*x**2 + 1*y**2 >=0     ** = "to the power of"
                                + m0*x0**e0 + m1*x1**e1 + m2*x2**e2 >= 0
           ( (m0, m1, m2) dot (1, x1, x2) ) >= 0

           Set characteristics of our unknown f() ie. coefficients and exponents
        */
        double m0 = 0;
        double m1 = 0;
        double m2 = 0;
        W_lcb f_coefficients = Eigen::VectorXd::Zero(vector_size);  // eigen also : << m0,m1,m2
        E_lcb f_exponents    = Eigen::VectorXd::Ones(vector_size);  //{1, 1, 1};  // only used: if algorithm == transformed_linear_ols_algorithm
        W_lcb w_lcb_weights  = Eigen::VectorXd::Zero(vector_size);  // usually set to zero vector [0]

        switch ( algorithm ) {
          case bias_variance_analysis_ols_algorithm :
            //               *** Mark the samples ***
            auto label_random_samples = [](X_lcb_sample &sample) {
                sample.y_true_sample_label = std::sin(grostig_pi * sample.x_lcb[0]);
                //                                                         cout << " y_sample_color: " << sample.y_true_sample_label << std::endl;
            };
//            f_coefficients << m0, m1, m2;  // eigen also : << m0,m1,m2
            std::for_each(d_ucs.begin(), d_ucs.end(), label_random_samples);
//            plot_graph_bias_variance( plot_canvas, "points on f(x)", d_ucs, f_coefficients, f_exponents );  // f_exponents not implemented todo note
            break;
/*
          case transformed_linear_ols_algorithm : {
              m0 = -0.4;         // Must be carefully specified to result in a sensible region.
              m1 = 1.0;        //
              m2 = 1.0;        //
              f_coefficients << m0, m1, m2;
              f_exponents << 1, 2, 2;        // only used: if algorithm == transformed_linear_ols_algorithm
//              *** Mark the samples with their true colors as we decided with our unknown f(). ***
              auto label_random_exp_samples = [&f_coefficients, f_exponents](X_lcb_sample &sample) {
                  sample.y_true_sample_label = label_sample_expnt_sign(f_coefficients, f_exponents, sample.x_lcb);
//                                                         cout << " y_sample_color: " << sample.y_true_sample_label << std::endl;
              };
              std::for_each(d_ucs.begin(), d_ucs.end(), label_random_exp_samples);
              plot_graph( plot_canvas, "After Labeling Points", d_ucs, f_coefficients, f_exponents );
              break;
            }
          default:
              //            m0 = my_distribution2(my_random_device2);        // can be any line intercept constant that fits domain
              //            m1 = my_distribution2(my_random_device2);        // can be any slope.
              //            m2 = my_distribution2(my_random_device2);        // can be any slope
              m0 = -1;        // can be any line intercept constant
              m1 = -7.0;        // can be any slope.
              m2 = 4.0;        // can be any slope
//               *** Mark the samples with their true colors as we decided with our unknown f(). ***
              auto label_random_samples =     [&f_coefficients](X_lcb_sample &sample) {
                  sample.y_true_sample_label = label_sample_dot_sign(f_coefficients, sample.x_lcb);
      //                                                         cout << " y_sample_color: " << sample.y_true_sample_label << std::endl;
              };
              f_coefficients << m0, m1, m2;  // eigen also : << m0,m1,m2
              std::for_each(d_ucs.begin(), d_ucs.end(), label_random_samples);
              plot_graph( plot_canvas, "After Labeling Points", d_ucs, f_coefficients, f_exponents );
              break;
*/
          }
        //        add_noise( d_ucs, per_cent_noise );  // todo put this in the correct case above with an if for the algorithm

//        *** Run Primary Algorithm
        switch ( algorithm ) {
          case bias_variance_analysis_ols_algorithm :
            w_lcb_weights = ordinary_least_squares_real(d_ucs);

            break;
/*
        case linear_ols_algorithm :
          case e_out_linear_ols_algorithm :
          case ols_to_perceptron_algorithm :
            w_lcb_weights = ordinary_least_squares_x3(d_ucs); // *** Run the OLS for one trial to calculate g(), ie. the weights. *** // mutates both variables.
            if (algorithm == e_out_linear_ols_algorithm) {
                historical_f_lcs.push_back(f_coefficients);
                historical_g_lcs.push_back(w_lcb_weights);
              }
            break;
          case perceptron_algorithm :   // do perceptron only.
            w_lcb_weights = perceptron(d_ucs, w_lcb_weights, f_exponents, algorithm_w_lcb_updates, plot_canvas); // *** Run the perceptron for one trial to calculate g(), ie. the weights. *** // mutates both variables.
            break;
          case transformed_linear_ols_algorithm :
            w_lcb_weights = ordinary_least_squares_x3(d_ucs);  // maybe this is part of the others similar todo
            break;
*/
          }

        /* Report Phase 1 Trial Results */
//        cout << "\n>> Boundary line equation//unknown f(x): " << m0 << ", " << m1 << ", " << m2 << " //Phase 1 weight vector: "<< w_lcb_weights.transpose() << std::endl;
//        double error_probabilty_samples = count_errors(f_coefficients, w_lcb_weights);
//        error_probabilty_samples_total += error_probabilty_samples;
//        double probability_of_error = error_probabilty_samples / (double)num_desired_propability_tests;
//        cout << ">> Trial #, probability of error: " <<num_trials<< ", " <<probability_of_error<<std::endl;
/*
        // Subsequent Algorithm, Within a Single Trial, if applicable
        if ( algorithm == ols_to_perceptron_algorithm ) {
            w_lcb_weights = ordinary_least_squares_x3( d_ucs );
            plot_graph( plot_canvas, "ols_to_perceptron_algorithm", d_ucs, w_lcb_weights, f_exponents );
            W_lcb w_lcb_temp = w_lcb_weights;
            w_lcb_weights = perceptron( d_ucs, w_lcb_weights, f_exponents, algorithm_w_lcb_updates, plot_canvas ); // *** Run the perceptron for one trial to calculate g(), ie. the weights. ***
            plot_graph( plot_canvas, "ols_to_perceptron_algorithm", d_ucs, w_lcb_weights, f_exponents );
            // Report Phase 2 Trial Results
            cout << "\n>>> Boundary line equation//OLS vector:  " << w_lcb_temp.transpose() << " //Phase 2 weight vector: "<< w_lcb_weights.transpose() << std::endl;
            double error_probabilty_samples = count_errors(f_coefficients, w_lcb_weights);
            error_probabilty_samples_total += error_probabilty_samples;
            double probability_of_error = error_probabilty_samples / (double)num_desired_propability_tests;
            cout << ">>> Trial #, probability of error: " <<num_trials<< ", " <<probability_of_error<<std::endl;
        }
*/
//        plot_graph_bias_variance( plot_canvas, "Final", d_ucs, w_lcb_weights, f_exponents );
        historical_g_lcs.push_back(w_lcb_weights);
        historical_d_ucs.push_back(d_ucs);
    }

//    double updates_per_trial2 = algorithm_w_lcb_updates / num_desired_trials;
//    cout << ">>> Updates per perceptron classification: " << updates_per_trial2 << std::endl;

    double g_mean = calc_g_mean(historical_g_lcs);
    double bias = 0;
    for (auto a_d_ucs:historical_d_ucs ) {  // calc bias_average for all trials
        for (auto i:a_d_ucs) {  // calc bias for one trial
            X_lcb a_x_lcb = i.x_lcb;
            double f_of_x = sin(grostig_pi*a_x_lcb[0]);
            bias += pow( (g_mean - f_of_x), 2 );
        }
    }
    double bias_average = bias/num_desired_trials/n_uc_num_samples;

    double variance = 0;
    double expected_error = 0;

    for (size_t i=0; i < num_desired_trials; i++) {
        D_ucs my_d_ucs = historical_d_ucs[i];
        W_lcb my_w_lcb = historical_g_lcs[i];
        for (auto my_sample:my_d_ucs) {  // calc variance for one trial
            double g_d_of_x    = my_sample.x_lcb[0];
            double g_mean_of_x = my_sample.x_lcb[0];
            variance += pow( ( g_d_of_x - g_mean_of_x), 2 );
        }

    }



    double variance_average = variance/num_desired_trials;

    cout << "\n g_mean, bias_average, variance_average: " << g_mean << ", " << bias_average << ", " << variance_average;


/*    double probability_of_error = error_probabilty_samples_total / (double)(num_desired_propability_tests * num_desired_trials);
    cout << ">>> Probability of error all trials: " << probability_of_error; cout << "\n###";

    if ( algorithm == e_out_linear_ols_algorithm  ||
         algorithm == transformed_linear_ols_algorithm ) { // not fully implemented todo
            for (long i = 0; i < num_desired_e_out_tests; i++) {
                D_ucs d_ucs_e_in;
                W_lcb f_coefficients = historical_f_lcs[i];
                // *** mark the samples with their true colors as we decided with our unknown f(). ***
                auto label_random_samples = [&f_coefficients](X_lcb_sample &sample) {
                    if (algorithm == transformed_linear_ols_algorithm) {

                    }
//                    sample.y_true_sample_label = label_sample_dot_sign(f_coefficients, sample.x_lcb);
//                                                         cout << " y_sample_color: " << sample.y_true_sample_label << std::endl;
                };
                std::for_each(d_ucs_e_in.begin(), d_ucs_e_in.end(), label_random_samples); // mark all my samples based on my hypothesis 'g'.  This makes them linearly separable
                ;
            }
        }
*/

    return 0;
};
