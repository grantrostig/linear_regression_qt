//
// Created by grostig on 10/11/17.
//

#pragma once
#include "my_main.h"
//#include "My_typedefs.h"  content of this file was needed if in Main.h, but not once My_typedefs.h were include in Main.h

class Linear_equation {  /*make virtual*/
public:
    C_lcb coefficients;
    V_lcb variables;
    E_lcb exponents;
    D_lcb divisors;
    Linear_equation();
    Linear_equation(const C_lcb c, const V_lcb v, const E_lcb e, const D_lcb d);
    virtual void dummy() = 0; /* required to make class virtual */
};
