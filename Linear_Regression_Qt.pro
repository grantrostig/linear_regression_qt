TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

# boost on fedora26 parial list only derived from Unbuntu list
LIBS += \
        -lboost_atomic \
        -lboost_chrono \
        -lboost_context \
        -lboost_coroutine \
        -lboost_date_time \
        -lboost_fiber \
        -lboost_filesystem \
#        -lboost_graph_parallel \
        -lboost_graph \
        -lboost_iostreams \
        -lboost_locale \
        -lboost_log_setup \
        -lboost_log \
        -lboost_math_c99f \
        -lboost_math_c99l \
        -lboost_math_c99 \
        -lboost_math_tr1f \
        -lboost_math_tr1l \
        -lboost_math_tr1 \
#        -lboost_mpi_python-py27 \
#        -lboost_mpi_python-py35 \
#        -lboost_mpi_python \
#        -lboost_mpi \
        -lboost_prg_exec_monitor \
        -lboost_program_options \
#        -lboost_python-py27 \
#        -lboost_python-py35 \
        -lboost_python \
        -lboost_random \
        -lboost_regex \
        -lboost_serialization \
        -lboost_signals \
        -lboost_system \
        -lboost_thread \
        -lboost_timer \
        -lboost_type_erasure \
        -lboost_unit_test_framework \
        -lboost_wave \
        -lboost_wserialization

# boost on ubuntu 17.04
#LIBS += \
#        -lboost_atomic \
#        -lboost_chrono \
#        -lboost_context \
#        -lboost_coroutine \
#        -lboost_date_time \
#        -lboost_fiber \
#        -lboost_filesystem \
#        -lboost_graph_parallel \
#        -lboost_graph \
#        -lboost_iostreams \
#        -lboost_locale \
#        -lboost_log_setup \
#        -lboost_log \
#        -lboost_math_c99f \
#        -lboost_math_c99l \
#        -lboost_math_c99 \
#        -lboost_math_tr1f \
#        -lboost_math_tr1l \
#        -lboost_math_tr1 \
#        -lboost_mpi_python-py27 \
#        -lboost_mpi_python-py35 \
#        -lboost_mpi_python \
#        -lboost_mpi \
#        -lboost_prg_exec_monitor \
#        -lboost_program_options \
#        -lboost_python-py27 \
#        -lboost_python-py35 \
#        -lboost_python \
#        -lboost_random \
#        -lboost_regex \
#        -lboost_serialization \
#        -lboost_signals \
#        -lboost_system \
#        -lboost_thread \
#        -lboost_timer \
#        -lboost_type_erasure \
#        -lboost_unit_test_framework \
#        -lboost_wave \
#        -lboost_wserialization


# Created by and for Qt Creator. This file was created for editing the project sources only.
# You may attempt to use it for building too, by modifying this file here.

#TARGET = Linear_Regression_Qt

HEADERS = \
   $$PWD/cppheaders.h \
   $$PWD/e_too_many_attempts.h \
   $$PWD/gnuplot-iostream.h \
   $$PWD/linear_equation.h \
   $$PWD/my_main.h \
   $$PWD/my_typedefs.h \
   $$PWD/slope_intercept_lin_eq.h \
   $$PWD/standard_form_lin_eq.h

SOURCES = \
   $$PWD/e_too_many_attempts.cpp \
   $$PWD/linear_equation.cpp \
   $$PWD/main.cpp \
   $$PWD/slope_intercept_lin_eq.cpp \
   $$PWD/standard_form_lin_eq.cpp

INCLUDEPATH = \
    $$PWD/.

#DEFINES = 

